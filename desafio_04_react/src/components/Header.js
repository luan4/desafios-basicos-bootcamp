import React, {Component}  from 'react';
import HeaderCss from './../style/Header.css'
import naruto from './../assets/naruto.png'
function Header  () {
  return  (
    <header  className="header">
      <h1 className="title" >Animes</h1>
      <div className="meuPerfil">
        <h3>Meu Perfil</h3>
        <img className="avatar" src={naruto} alt=""/>
      </div>
    </header>
  );
}

export default Header;