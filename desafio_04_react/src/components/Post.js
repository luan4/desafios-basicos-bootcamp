import React, { Component } from 'react';
import Comment from './Comment';

function Post({ post }) {

  return (


    <div className="post">

      <div className="header" >
        <img className="avatar" src={post.author.avatar} alt="" />
        <div>
          <h4 className="nome">{post.author.name}</h4>
          <p className="data">{post.date}</p>
        </div>
      </div>

      <div className="content-pergunta">
        <p className="pergunta">{post.content}</p>
      </div>

      {post.comments.map(comment => 
        <Comment key={comment.id} comment={comment}></Comment>
      )}
    </div>
  );

}

export default Post;

