import React, { Component } from 'react';

function Comment({ comment }) {
  return (
    <div className="resposta"  >
      <div>
        <img className="avatar-resposta" src={comment.author.avatar} alt="" />
      </div>
      <div className="texto">
        <p> {comment.author.name}: {comment.content} </p>
      </div>
    </div>
  )
}

export default Comment;
