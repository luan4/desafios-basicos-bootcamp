import React, { Component } from 'react';
import PostListcss from './../style/PostListcss.css'
import naruto from './../assets/naruto.png'
import deku from './../assets/deku.jpeg'
import bakugo from './../assets/bakugo.jpg'
import Post from './../components/Post';
class PostList extends Component {

  state = {
    posts: [
      {
        id: 1,
        author: {
          name: "Naruto",
          avatar: naruto
        },
        date: "28/10/2019",
        content: "Pessoal, já saiu o ep de 4 my hero academia?",
        comments: [
          {
            id: 1,
            author: {
              name: "Deku",
              avatar: deku
            },
            content: "ainda não, só sábado."
          },
          {
            id: 2,
            author: {
              name: "Naruto",
              avatar: naruto
            },
            content: "Vlw, Deku!"
          }
        ]
      },
      {
        id: 2,
        author: {
          name: "Deku",
          avatar: deku
        },
        date: "27/10/2019",
        content: "alguém viu o Kacchan ???",
        comments: [
          {
            id: 1,
            author: {
              name: "Bakugo",
              avatar: bakugo
            },
            content: "Deku, Já falei pra não me chamar assim. "
          },
          {
            id: 2,
            author: {
              name: "Bakugo",
              avatar: bakugo
            },
            content: "Eu vou te matar!"
          }
        ]
      }
    ]
  };

  render() {

    this.state.posts.map(post => console.log(post));

    return (

      <section className="timeline">
        {this.state.posts.map(post => <Post key={post.id} post={post} ></Post>)}
      </section>

    )
  };
}

export default PostList;