import React from 'react';
import Header from './components/Header'
import './App.css'
import profile from './assets/curso.png'
import PostList from './components/PostList';
function App (){
  return (
  <>
  <Header></Header>
  <PostList></PostList>
  </>
  )
}

export default App;